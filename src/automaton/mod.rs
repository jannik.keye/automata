pub trait Tick {
    fn tick(&mut self);
}

pub trait Draw {
    fn draw(&self, window: &mut piston_window::PistonWindow, window_event: &piston_window::Event);
}