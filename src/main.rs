extern crate rand;
extern crate piston_window;
extern crate rayon;

use piston_window::*;

use std::rc::Rc;

mod automaton;
mod game_of_life;
mod graphics;
mod predator_and_prey;
mod util;

use std::fmt;
use automaton::*;
use game_of_life::GameOfLife;
use predator_and_prey::PredatorAndPrey;
use std::thread::sleep;
use std::time::Duration;

pub const CELL_SIZE: f64 = 10.0;

fn clear_terminal() {
    print!("\x1b[2J");
}

enum Automaton {
    PredatorAndPrey(PredatorAndPrey),
    GameOfLife(GameOfLife),
}

impl Tick for Automaton {
    fn tick(&mut self) {
        match self {
            Automaton::PredatorAndPrey(pap) => pap.tick(),
            Automaton::GameOfLife(gol) => gol.tick(), 
        }
    }
}

fn main() {
    clear_terminal();
    println!("
        Hi, there!\n
        Choose a simulation:\n
        a: Predator and Prey
        b: Conways Game Of Life
    ");
    print!("Pick an option: ");
    let mut option = String::new();


    match std::io::stdin().read_line(&mut option) {
        Ok(_line) => option = option.replace("\n", ""),
        Err(e) => println!("{}", e)
    }


    let dimensions: [u32; 2] = [35 * CELL_SIZE as u32, 35 * CELL_SIZE as u32];
    let width = dimensions[0].clone();
    let height = dimensions[1].clone();
    let mut steps: usize = 0;
    let mut simulation = match option.as_ref() {
        "a" => Automaton::PredatorAndPrey(PredatorAndPrey::new(width / CELL_SIZE as u32, height / CELL_SIZE as u32)),
        "b" => Automaton::GameOfLife(GameOfLife::new(width, height)),
        _ => Automaton::PredatorAndPrey(PredatorAndPrey::new(width, height))
    };

    let mut events = Events::new(
        EventSettings::new().ups(30)
    );

    events.ups(30);

    let mut window: PistonWindow = WindowSettings::new("Hello Piston!", (width, height))
        .exit_on_esc(true)
        .build()
        .unwrap_or_else(|e| { panic!("Failed to build PistonWindow: {}", e) });

    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            window.draw_2d(&e, |context, graphics, _d| {
                clear([0.0, 0.0, 0.0, 1.0], graphics);
    
                match &simulation {
                    Automaton::PredatorAndPrey(pap) => {
                        pap.creatures.lock().unwrap().iter().for_each(|c| {
                            piston_window::rectangle(c.color.to_f32_array(), [c.x as f64 * CELL_SIZE, c.y as f64 * CELL_SIZE, 1.0 * CELL_SIZE, 1.0 * CELL_SIZE], context.transform, graphics);
                        });
                    },
                    Automaton::GameOfLife(gol) => {
                        gol.cells.iter().enumerate().for_each(|(index, c)| {
                            piston_window::rectangle(
                                if c.is_alive() { [1.0, 0.0, 0.0, 1.0] } else {
                                    [0.0, 0.0, 0.0, 1.0]
                                }
                            , [(index % gol.width as usize) as f64 * CELL_SIZE, ((index % gol.width as usize) as f64).floor() * CELL_SIZE, 1.0 * CELL_SIZE, 1.0 * CELL_SIZE], context.transform, graphics);
                        });
                    },
                }
            });
        }

        if let Some(args) = e.update_args() {
            simulation.tick();
        }
    }

}
