mod creature;

use std::sync::{Arc, Mutex};
use rayon::prelude::*;

use automaton::{Tick, Draw};
use util::*;

use self::creature::Creature;
use std::fmt;

pub struct PredatorAndPrey {
    pub creatures: Arc<Mutex<Vec<Creature>>>,
    pub creature_count: usize,
    pub width: u32,
    pub height: u32,
}

impl PredatorAndPrey {
    pub fn new(width: u32, height: u32) -> Self {
        let cells = width * height;
        let mut creatures = Vec::new();

        for i in 0..cells {
            let x = i % width;
            let y = i / width;

            creatures.push(Creature::new(x, y));
        }

        PredatorAndPrey {
            creature_count: (width * height) as usize,
            creatures: Arc::new(Mutex::new(creatures)),
            width,
            height,
        }
    }
}

impl Tick for PredatorAndPrey {
    fn tick(&mut self) {
        (0..self.creature_count).into_par_iter().for_each(|i| {
            let mut creatures = self.creatures.lock().unwrap();
            let (first, second) = mut_two::<Creature>(
                i,
                get_other_index(i as u32, self.width, self.height) as usize,
                &mut creatures,
            );

            first.tick(second);
        });
    }
}